import 'package:cloud_firestore/cloud_firestore.dart';

class DatabaseMethods{
  createUser(userMap, uid) {
    Firestore.instance.collection("Users").document(uid).setData(userMap);
  }
}