import 'package:flutter/material.dart';

SnackBar errorSnackBar(String text) {
  return SnackBar(
    content: Text(text),
    backgroundColor: Colors.red,
  );
}

SnackBar successSnackBar(String text) {
  return SnackBar(
    content: Text(text),
    backgroundColor: Colors.green,
  );
}

SnackBar processSnackBar(String text) {
  return SnackBar(
    content: Text(text),
    backgroundColor: Colors.orange,
  );
}

InputDecoration textFieldInputDecoration(String hintText, IconData iconData) {
  return InputDecoration(
      hintText: hintText,
      hintStyle: TextStyle(color: Colors.black26),
      focusedBorder: UnderlineInputBorder(
          borderSide: BorderSide(color: Color(0xFF0088cc))),
      enabledBorder:
      UnderlineInputBorder(borderSide: BorderSide(color: Colors.black26)),
      icon: Icon(
        iconData,
        color: Colors.black26,
      ));
}

InputDecoration textFieldInputDecorationWithoutIcon(String hintText) {
  return InputDecoration(
    hintText: hintText,
    hintStyle: TextStyle(color: Colors.black26),
    focusedBorder:
    UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
    enabledBorder:
    UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
  );
}

InputDecoration textFieldInputDecorationWithoutIconWithoutUnderLineBorder(String hintText) {
  return InputDecoration(
    hintText: hintText,
    hintStyle: TextStyle(color: Colors.black26),
    focusedBorder:
    UnderlineInputBorder(borderSide: BorderSide(color: Colors.white)),
    enabledBorder:
    UnderlineInputBorder(borderSide: BorderSide(color: Colors.black26)),
  );
}
