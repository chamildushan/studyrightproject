import 'package:flutter/material.dart';
import 'package:studyRight/screens/home.dart';
import 'package:studyRight/widgets/icon_badge.dart';
import 'package:studyRight/screens/chat_UI.dart';

class MainScreen extends StatefulWidget {
  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  PageController _pageController;
  int _page = 0;
  int _currentIndex=0;
  final List<Widget> _children = [
    ChatScreen(),
    Home()

  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: PageView(
        physics: NeverScrollableScrollPhysics(),
        controller: _pageController,
        onPageChanged: onPageChanged,

        children:List.generate(4, (index) => Home()),



      ),
      bottomNavigationBar: BottomAppBar(
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            SizedBox(width: 7.0),
            barIcon(icon: Icons.home, page: 0,),
            barIcon(icon: Icons.favorite, page: 1),
            barIcon(icon: Icons.mode_comment, page: 2, badge: true),
            barIcon(icon: Icons.person, page: 3),
            SizedBox(width: 7.0),
          ],
        ),
        color: Theme.of(context).primaryColor,
      ),
    );
  }

  void navigationTapped(int page) {
    _pageController.jumpToPage(page);
  }

  @override
  void initState() {
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  void onPageChanged(int page) {
    setState(() {
      this._page = page;
    });
  }

  Widget barIcon(
      {IconData icon = Icons.home, int page = 0, bool badge = false}) {
    return IconButton(
      icon: badge ? IconBadge(icon: icon, size: 24.0) : Icon(icon, size: 24.0),
      color:
      _page == page ? Theme
          .of(context)
          .accentColor : Colors.blueGrey[300],
      onPressed: () => _pageController.jumpToPage(page),
    );
  }
    /*Widget barIcon1(
        {IconData icon = Icons.mode_comment, int page = 2, bool badge = true}) {
      return IconButton(
        icon: badge ? IconBadge(icon: icon, size: 24.0) : Icon(icon, size: 24.0),
        color:
        _page == page ? Theme.of(context).accentColor : Colors.blueGrey[300],
        onPressed: () => _pageController.jumpToPage(page),
      );
  }*/
}
