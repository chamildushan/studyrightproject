import 'package:flutter/material.dart';
import 'package:studyRight/screens/chat_UI.dart';
import 'package:studyRight/screens/coursedetails.dart';
import 'package:studyRight/screens/details.dart';
import 'package:studyRight/screens/home.dart';
import 'package:studyRight/screens/loginPage.dart';
import 'package:studyRight/screens/main_screen.dart';
import 'package:studyRight/screens/navBarimpl.dart';
import 'package:studyRight/util/const.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Constants.appName,
      theme: Constants.lightTheme,
      darkTheme: Constants.darkTheme,
      home: LoginPage(),
    );
  }
}