List coursesSLIIT = [
  {//the data for the app is passed through here
    "name": "BSc in IT ",
    "img": "assets/2.jpg",
    "price": r"Rs.10,500",
    "location": "Part time",
    "details": "The Ceylon-German Technical Training Institute (CGTTI)\n "
        " is the foremost institute in Sri Lanka for the training of\n"
        "  skilled technicians in the field of Automobile Engineering and allied trades." "\n"
        "The institute was originally established in 1959 at the premises of the central workshops of the Transport Board of Sri Lanka at Werahera.",
  },
  {
    "name": "BSc in SE ",
    "img": "assets/2.jpg",
    "price": r"Rs.22,000",
    "location": "Full time",
    "details": "The Ceylon-German Technical Training Institute (CGTTI)\n "
        " is the foremost institute in Sri Lanka for the training of\n"
        "  skilled technicians in the field of Automobile Engineering and allied trades." "\n"
        "The institute was originally established in 1959 at the premises of the central workshops of the Transport Board of Sri Lanka at Werahera.",
  },
  {
    "name": "Management",
    "img": "assets/2.jpg",
    "price": r"$",
    "location": "Colombo 01",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\n\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
  {
    "name": "HR ",
    "img": "assets/4.jpg",
    "price": r"$$$",
    "location": "Malabe",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
  {
    "name": "Enigneering",
    "img": "assets/2.jpg",
    "price": r"$$",
    "location": "Galle Rd, Galkissa",
    "details": "Pellentesque in ipsum id orci porta dapibus. "
        "Nulla porttitor accumsan tincidunt. Donec rutrum "
        "congue leo eget malesuada. "
        "\nPraesent sapien massa, convallis a pellentesque "
        "nec, egestas non nisi. Donec rutrum congue leo eget malesuada. "
        "Mauris blandit aliquet elit, eget tincidunt nibh pulvinar a. "
        "Sed porttitor lectus nibh. Donec sollicitudin molestie malesuada. "
        "\nCurabitur arcu erat, accumsan id imperdiet et, porttitor at sem. "
        "Vestibulum ac diam sit amet quam vehicula elementum sed sit amet dui.",
  },
];